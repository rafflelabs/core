<?php

namespace LaravelComponents\Core\Interfaces;


interface EntityBaseManipulation
{
    /**
     * Get all entities
     *
     * @return mixed
     */
    public function getAll();

    /**
     * Get entity by ID
     *
     * @param integer $entity_id
     * @return mixed
     */
    public function getByID($entity_id);

    /**
     * Create new entity
     *
     * @param array $entity_data
     * @return mixed
     */
    public function create(array $entity_data);

    /**
     * Update entity by ID
     *
     * @param integer $entity_id
     * @param array $entity_data
     * @return mixed
     */
    public function update($entity_id, array $entity_data);

    /**
     * Destroy entity by ID
     *
     * @param integer $entity_id
     * @return mixed
     */
    public function destroy($entity_id);

    /**
     * Destroy all entities
     *
     * @return mixed
     */
    public function destroyAll();
}