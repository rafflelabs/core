<?php

namespace LaravelComponents\Core\Traits;

use Illuminate\Database\Eloquent;

trait EntityBaseManipulation
{
    /**
     * Need set entity class for manipulataion
     */
//    protected $entity_class = \LaravelComponents\Ecommerce\Entities\Category::class;

    /**
     * Return entity
     *
     * @return \Illuminate\Database\Eloquent
     */
    protected function getEntity(){
        return !isset($this->entity_class) ? $this : new $this->entity_class();
    }

    /**
     * Get all entities
     *
     * @return mixed
     */
    public function getAll(){
        $entity = $this->getEntity();
        return $entity->all();
    }

    /**
     * Get entity by ID
     *
     * @param integer $entity_id
     * @return mixed
     */
    public function getByID($entity_id){
        $entity = $this->getEntity();
        return $entity->find($entity_id);
    }

    /**
     * Create new entity
     *
     * @param array $entity_data
     * @return mixed
     */
    public function create(array $entity_data){
        $entity = $this->getEntity();
        return $entity->create($entity_data);
    }

    /**
     * Update entity by ID
     *
     * @param integer $entity_id
     * @param array $entity_data
     * @return mixed
     */
    public function update($entity_id, array $entity_data){
        $entity = $this->getEntity();
        return  $entity->find($entity_id)->update($entity_data);
    }

    /**
     * Destroy entity by ID
     *
     * @param integer $entity_id
     * @return mixed
     */
    public function destroy($entity_id){
        $entity = $this->getEntity();
        return  $entity->destroy($entity_id);
    }

    /**
     * Destroy all entities
     *
     * @return mixed
     */
    public function destroyAll(){
        $entity = $this->getEntity();
        return  $entity->truncate();
    }

    /**
     * Get from DB and prepare array with ID as key and name as value
     *
     * @return mixed
     */
    public function getForSelect()
    {
        $entity = $this->getEntity();

        if(!$entities = $entity->select('id' , 'name')->orderBy('name')->get()){
            return $entities;
        }

        $result = [];
        foreach($entities as &$entity){
            $result[$entity['id']] = $entity['name'];
        }

        return $result;
    }
}