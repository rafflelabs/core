<?php

namespace LaravelComponents\Ecommerce\Services;


use LaravelComponents\Core\Interfaces\EntityBaseManipulation as EntityBaseManipulationInterface;
use LaravelComponents\Core\Traits\EntityBaseManipulation;


abstract class EntityService implements EntityBaseManipulationInterface
{
    use EntityBaseManipulation;

    /**
     * Use for getting current entity to trait
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected abstract function getEntity_class();
}